﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kg3_1
{
    public partial class Form1 : Form
    {
        Bitmap bitmap;
        Point[] pointF;

        public class Pixel
        {
            public int x = 0;
            public int y = 0;
            public Color color = Color.Black;
            public Pixel(int x, int y, Color color)
            {
                this.x = x;
                this.y = y;
                this.color = color;
            }
           

        }

        public void clearScreeen()
        {
            Graphics g = Graphics.FromImage(bitmap);
            g.Clear(Color.White);
            pictureBox1.Image = bitmap;
        }

        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            clearScreeen();
            //XocheshXochesh();
            //fillRect();

        }

        public void CreateRect()
        {
            clearScreeen();
            Pen pen = new Pen(Color.Black);
            pointF = new Point[5];

            pointF[0].X = 50;
            pointF[0].Y = 50;

            pointF[1].X = 150;
            pointF[1].Y = 50;

            pointF[2].X = 150;
            pointF[2].Y = 150;

            pointF[3].X = 50;
            pointF[3].Y = 150;

            pointF[4].X = 50;
            pointF[4].Y = 50;

            Graphics g = Graphics.FromImage(bitmap);
            g.DrawPolygon(pen, pointF);
            pictureBox1.Image = bitmap;
        }

        public void createRomb()
        {

        }

        public void CreateFigure1()
        {
            clearScreeen();
            Pen pen = new Pen(Color.Black);
            pointF = new Point[7];
            pointF[0].X = 50;
            pointF[0].Y = 50;

            pointF[1].X = 150;
            pointF[1].Y = 100;

            pointF[2].X = 200;
            pointF[2].Y = 70;

            pointF[3].X = 200;
            pointF[3].Y = 120;

            pointF[4].X = 100;
            pointF[4].Y = 200;

            pointF[5].X = 50;
            pointF[5].Y = 120;

            pointF[6].X = 50;
            pointF[6].Y = 50;




            Graphics g = Graphics.FromImage(bitmap);
            g.DrawPolygon(pen, pointF);
            pictureBox1.Image = bitmap;
        }

        public void simpleFillRect()
        {
            Pixel curPixel;
            int x = 100;
            int y = 80;
            Color gran = Color.Black;
            Stack<Pixel> pixels = new Stack<Pixel>();
            pixels.Push(new Pixel(x, y, Color.Black));
            while(pixels.Count != 0)
            {
               curPixel = pixels.Pop();
         
                if(bitmap.GetPixel(curPixel.x + 1, curPixel.y).ToArgb() != gran.ToArgb())
                   pixels.Push(new Pixel(curPixel.x + 1, curPixel.y, Color.Black));

                if (bitmap.GetPixel(curPixel.x - 1, curPixel.y).ToArgb() != gran.ToArgb())
                    pixels.Push(new Pixel(curPixel.x - 1, curPixel.y, Color.Black));

                if (bitmap.GetPixel(curPixel.x, curPixel.y + 1).ToArgb() != gran.ToArgb())
                    pixels.Push(new Pixel(curPixel.x, curPixel.y + 1, Color.Black));

                if (bitmap.GetPixel(curPixel.x, curPixel.y - 1).ToArgb() != gran.ToArgb())
                    pixels.Push(new Pixel(curPixel.x, curPixel.y - 1, Color.Black));
               
                bitmap.SetPixel(curPixel.x, curPixel.y, curPixel.color);
            }

            pictureBox1.Image = bitmap;
        }

       

        public void fillMnog()
        {
            List<TR> tr = new List<TR>();
            List<TR> tra = new List<TR>();
            List<float> list = new List<float>();
            for(int i = 0; i < pointF.Length; i++)
            {
                if (i + 1 >= pointF.Length) break;
                if (pointF[i].Y == pointF[i + 1].Y) continue;
                tr.Add(new TR(i, pointF[i].X, pointF[i].Y, pointF[i + 1].X, pointF[i + 1].Y));
            }
            /*
            int flag1 = 0, flag2 = 0;
            int y1, y2;
            
            for(int i = 0; i < tr.Count; i++)
            {
                if (i + 1 >= tr.Count) break;

                if (tr[i].xMin > tr[i].xMax)
                {
                    tr[i].m *= -1;
                    flag1 = 1;
                }

                if (tr[i + 1].xMin > tr[i + 1].xMax)
                {
                    tr[i + 1].m *= -1;
                    flag2 = 1;
                }
                
                if (tr[i].yMin < tr[i].yMax)
                    y1 = -1;
                else y1 = 1;


                if (tr[i + 1].yMin < tr[i + 1].yMax)
                    y2 = -1;
                else y2 = 1;


                if(!(tr[i].yMax < tr[i].yMax + y1 && tr[i].yMax < tr[i + 1].yMin + y2) || !(tr[i].yMax > tr[i].yMax + y1 && tr[i].yMax > tr[i + 1].yMin + y2))
                {
                    tr[i].yMax += y1;
                    tr[i].xMax -=(int)tr[i].m;
                }

            }
            
            for (int i = 0; i < tr.Count; i++)
            {
                MessageBox.Show(tr[i].ToString());
            }
            */

            for (int i = 0; i < tr.Count; i++)
            {
                tr[i].sort();
                
            }
            MessageBox.Show(tr.Count.ToString());
            for(int i = 0; i < tr.Count; i++)
            {
                MessageBox.Show(tr[i].ToString());
            }
           

            for(int i = 50; i < pictureBox1.Height; i++)
            {
                for(int j = 0; j < tr.Count; j++) //добавление отрезков
                {
                    if (tr[j].yMin <= i) {
                        tra.Add(tr[j]);
                        tr.Remove(tr[j]);
                        j--;
                   }
                }

                for(int j = 0; j < tra.Count; j++) //удаление 
                {
                    if(tra[j].yMax <= i)
                    {
                        tra.Remove(tra[j]);
                        j--;
                    }
                }
                tra = SortX(tra); //сортировка

                

                //MessageBox.Show(tra.Count.ToString());
                for(int j = 0; j < tra.Count; j+= 2)
                {
                    
                    if (j + 1 >= tra.Count) break;
                    //MessageBox.Show("отрисовываем интервал: " + tra[j].xMin.ToString() + " " +  tra[j + 1].xMin.ToString());
                    drawInterwal((int)tra[j].xMin, (int)tra[j + 1].xMin, i);
                    tra[j].xMin += tra[j].m;
                    tra[j + 1].xMin += tra[j + 1].m;

                }

                if (tra.Count == 0 && tr.Count == 0) break;
            }
            pictureBox1.Image = bitmap;
        }


        public List<TR> SortX(List<TR> tra)
        {

            TR tr;
            for (int i = 0; i < tra.Count; i++)
            {
                for (int j = i + 1; j < tra.Count; j++)
                {
                    if (tra[i].xMin > tra[j].xMin)
                    {
                        tr = tra[i];
                        tra[i] = tra[j];
                        tra[j] = tr;
                    }
                }

            }
            return tra;
        }

        public void drawInterwal(int x1, int x2, int i)
        {
             while(x1 <= x2)
            {
                bitmap.SetPixel(x1, i, Color.Black);
                x1++;
            }

        }

        public class TR
        {
            public int i;
            public int yMin;
            public int yMax;
            public float xMin;
            public int xMax;
            public float m;
            public TR(int i, int x0, int y0, int xk, int yk)
            {
                this.i = i;
                xMin = x0;
                yMin = y0;
                yMax = yk;
                xMax = xk;
                m = (float)(xMax - xMin) / (yMax - yMin);
               
            }
            public void sort()
            {
                /*if (xMin < xMax && yMin > yMax)
                {
                    int tmp = 0;
                    tmp = (int)xMin;
                    xMin = xMax;
                    xMax = tmp;
                }
                */
                if (yMin > yMax)
                {
                    int tmp = 0;
                    tmp = yMin;
                    yMin = yMax;
                    yMax = tmp;
                    tmp = (int)xMin;
                    xMin = xMax;
                    xMax = tmp;
                }

            }

            public override string ToString()
            {
                string a = "номер = " + i + " " + xMin + " " + yMin + " "+ xMax + " " + yMax + " " + m; 
                return a;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Equals("Квадрат")) CreateRect();
            if (comboBox1.Text.Equals("Ромб")) ;
            if (comboBox1.Text.Equals("Произвольный пятиугольник 1")) CreateFigure1();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text.Equals("Алгоритм растровой развертки многоугольников")) fillMnog();
            if (comboBox2.Text.Equals("Четырехсвязный г-о")) simpleFillRect();
        }
    }
}
